<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('theme_id');
            $table->string('user_id');
            $table->text('article');
            $table->string('photo');
            $table->string('video');
            $table->string('audio');
            $table->string('file');
            $table->string('url');
            $table->enum('visible_all', array('1', '0',))->default('1');
            $table->enum('visible_friends', array('1', '0',))->default('1');
            $table->enum('visible_groups', array('1', '0',))->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contents');
    }
}
