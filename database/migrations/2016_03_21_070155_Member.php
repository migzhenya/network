<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Member extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('members',function (Blueprint $table){
		 $table->increments('id');
		 $table->integer('user_id')->references('id')->on('users');
		 $table->integer('group_id')->references('id')->on('groups');
		 $table->integer('referer_id')->references('id')->on('users');
		 $table->timestamps();		 
		   
	   });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::drop('members');
    }
}
