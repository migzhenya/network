<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Topics extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
  Schema::create('topics', function (Blueprint $table) {
	  $table->increments('id');
	  $table->string('user_id');
      $table->string('keywords');
      $table->enum('showhide', array('show', 'hide'))->default('show');
	  $table->string('name');
	  $table->string('description');
	  $table->timestamps();
	  
	});
	}
    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
     Schema::drop('topics');
    }
}
