<?php
use App\Topic;

use Illuminate\Database\Seeder;

class TopicSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     Topic::create(array('name'=>'введение','description'=>'Для чего нужны каскадные таблицы стилей'));
    }
}
