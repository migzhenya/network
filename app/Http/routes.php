<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware'=>['web','auth']],function(){
Route::controllers([
'group'=>'Cabinet\GroupController',
"cabinet"=>"Cabinet\CabinetController",
'themes'=>'Cabinet\ThemesController',
'usercontents'=>'Cabinet\UsercontentsController',
'comments'=>'Cabinet\CommentController',
'articles'=>'Cabinet\ArticleController',
'friends'=>'Cabinet\FriendController',

]);
    Route::auth();

 
 Route::get('/search.results', 'SearchController@getResults');

 Route::get('/search.friendsresults', 'SearchController@getFriendResults');

});


Route::group(['middleware' => ['web',]], function () {

Route::controllers([
'groups'=>'GroupsController',
'themesForGuest'=>'ThemesForGuestController',
'guestContents'=>'GuestContentsController',
'{id}'=>'AccountController',
]);

	Route::auth();
  
 

    Route::get('/','BaseController@index');


   });


