<?php

namespace App\Http\Controllers\Cabinet;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Theme;
use Input;
use Auth;

class ThemesController extends Controller
{
	public function getIndex() {
		$themes = Theme::all();
		return view('themes.themes')->with('themes', $themes);
	}

	public function getOnetheme($slug) {
		$oneTheme = Theme::latest('slug')->where('slug', '=', $slug)->get();
	 	return view('Themes.oneTheme')->with("oneTheme", $oneTheme);
	}

	public function postAddtheme(Requests\ThemesRequest $request) {
		$request['user_id'] = Auth::user()->id;
		Theme::Create($request->all());
        return redirect("themes");
	}

	public function getDelete($id = NULL) {
		$article = Theme::find($id);
		$article->delete();
		return redirect('themes');
}
	public function postEdit($id = null) {
	 $edit_article = Theme::find($id);
	 return view('Themes.edit_theme')->with("edit_article", $edit_article);
	}

}