<?php

namespace App\Http\Controllers\Cabinet;

use App\Http\Controllers\Controller;
use App\Account;
use Illuminate\Http\Request;
use App\Http\Requests\GroupRequest;
use App\Http\Requests;
use Auth;
use App\Group;
use App\Member;
class GroupController extends Controller
{
   public function getIndex() {
   $personal = Account::where('user_id', Auth::user()->id)->first();
     if(!$personal) {
         $personal = new Account();
     }
    $url = $_SERVER['REQUEST_URI'];
   $groups=Group::where('user_id',Auth::user()->id)->get();   
   return view("group.main")->with('groups',$groups)->with('url', $url)->with('personal', $personal);
   }
   public function postIndex(GroupRequest $r){
   $r['user_id']=Auth::user()->id;
   
   Group::create($r->all());  
   return redirect('group');  
   }
	public function getOne($id)  {
		$one=Group::find($id);
		return view('members')->with('one',$one);
	} 
	public function getConfirm($id){
		$one=Member::find($id);
		if (Auth::user()->id==$one->owner_id){
			$up=member::find($one->id);
			$up->status='confirmed';
			$up->save();
			return redirect ('groups');
		}
		
	}  
		
}
