<?php

namespace App\Http\Controllers\Cabinet;

use App\Http\Controllers\Controller;
use App\Theme;
use App\Content;
use App\Http\Requests;

use Request;
use Input;
use Image;
use Auth;

class UsercontentsController extends Controller
{
	public function getIndex() {
		$themes = Theme::where('user_id', Auth::user()->id)->get();
		return view('Contents.UserContents')->with('themes', $themes);
	}

	public function postIndex(Requests\ContentsRequest $request) {
		$request['theme_id'] = Request::input('theme_for_contents');

		$request['user_id'] = Auth::user()->id;

		$photo = \App::make('\App\libs\imag')->url($_FILES['photografy']['tmp_name'], '/media/uploads/user_photo/'.Auth::user()->id.'/', Auth::user()->id);
       if($photo) {
        $request['photo'] = $photo;
       }

       

		Content::Create($request->all());
		return redirect('usercontents');

	}
}
