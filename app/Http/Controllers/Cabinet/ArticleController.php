<?php

namespace App\Http\Controllers\Cabinet;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Theme;
use App\Article;
use Input;
use Auth;


class ArticleController extends Controller
{
	public function getIndex($theme_id) {
		$articles = Article::all()->where('theme_id', $theme_id);
		return view('Articles.allArticles')->with('articles', $articles);
	}

	public function postIndex(Requests\ArticleRequest $request, $theme_id) {
		$request['user_id'] = Auth::user()->id;
		$request['theme_id'] = $theme_id;
		Article::Create($request->all());
        return redirect("articles");
	}
	
}
