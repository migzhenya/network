<?php

namespace App\Http\Controllers\Cabinet;

use App\Http\Controllers\Controller;
use App\Comment;
use App\Http\Requests;

use Request;
use Input;
use Image;
use Auth;


class CommentController extends Controller
{
	public function postComment(Requests\CommentRequest $request, $theme_id) {
		$request['user_id'] = Auth::user()->id;
		$request['theme_id'] = $theme_id;
		$photo = \App::make('\App\libs\imag')->url($_FILES['photo']['tmp_name'], '/media/uploads/comment_photo/'.Auth::user()->id.'/', Auth::user()->id);
       if($photo) {
        $request['picture'] = $photo;
       }
		Comment::Create($request->all());
		return redirect('themes');
	}
}
//'.$theme_name