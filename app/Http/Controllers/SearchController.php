<?php

namespace App\Http\Controllers;

use App\Group;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;

class SearchController extends Controller
{
  public function getResults(Request $request) {        
         $query = $request->input('query');
	
         if (!$query) {
         	return redirect('groups.groups');
         }

         $groups = Group::where('name', '=', "{$query}")
                  ->get();
               
		return view('search.results')->with('groups', $groups);
	}
	public function getFriendResults(Request $request) {        
         $query = $request->input('query');
	
         if (!$query) {
         	return redirect('friends.main');
         }

         $users = User::where('name', '=', "{$query}")
                  ->get();
               
		return view('search.friendsresults')->with('users', $users);
	}
}
