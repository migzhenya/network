<?php

namespace App\Http\Controllers;
use App\User;
use App\Group;
use App\Theme;
use Illuminate\Http\Request;

use App\Http\Requests;

class AccountController extends Controller
{
    public function getIndex($id){
		$ac=User::where('name',$id)->first();
		$groups=Group::where('user_id',$ac->id)->where('showhide','show')->get();

		return view('account.account')->with('ac',$ac)->with('groups',$groups);
	}
	public function getAll(){
		$all=User::paginate(100);
		return view('account.allaccounts')->with('all',$all);
	}
	
}
