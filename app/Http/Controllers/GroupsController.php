<?php

namespace App\Http\Controllers;
use App\Member;
use App\Group;

use Auth;
use Illuminate\Http\Request;

use App\Http\Requests;

class GroupsController extends Controller
{
   public function getIndex(){
	   $all=Group::where('showhide','show')->get();
	  return view ("groups.groups")->with('all',$all);
   }
	public function getOne($id){
		$one=Group::find($id);
		foreach($one->members as $mem){
			$mem_user=Member::where('group_id',$one->id)->where('user_id',Auth::user()->id)->first();
			 
			if(isset($mem_user->user_id)){
				$mm = $mem_user->status;
			}
		
		}
		if(!isset($mm)){
			$mm = false;
		} 
	   return view('groups.one')->with('one',$one)->with('mm',$mm);
}
	public function getAdd($id){
				$one=Group::find($id);
		$thema="Вам поступило сообщение с сайта";
		
		$body="Пользователь <a href='#'> ".Auth::user()->name."</a>Хотите присоединиться к группе". $one->name;
		mail($one->users->email,$thema,$body);
		$gg=new Member;
		$gg->owner_id =$one->users->id;
		$gg->user_id =Auth::user()->id;
		$gg->status='new';
		$gg->group_id =$one->id;
		$gg->save();
		return redirect ('groups');
	}
	public function getDelete($id){
			$one=Member::find($id);
			$one->delete();
			return redirect ('groups');
		}
	
	
	
	
}
