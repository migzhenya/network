<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = array('user_id', 'theme_id', 'comment', 'body_comment', 'picture');
}
