<?php

namespace App\Providers;

use View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        
        View::composer('cabinet.*', 'App\Providers\ViewComposers\CabinetComposer');
        View::composer('Themes.*', 'App\Providers\ViewComposers\CabinetComposer');

        View::composer('Themes.*', 'App\Providers\ViewComposers\ThemesComposer');

        View::composer('*', 'App\Providers\ViewComposers\SiteComposer');

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
