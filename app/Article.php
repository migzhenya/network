<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable=['name','body', 'keywords', 'slug', 'user_id', 'theme_id'];

   public function users() {
   	return $this->belongsTo('App\User', 'user_id');
   }
}
