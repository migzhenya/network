<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
   protected $fillable = array('theme_id', 'article', 'photo', 'video', 'audio', 'file', 'url', 'user_id');
}
