<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Theme extends Model
{
   protected $fillable=['name','description', 'keywords', 'slug', 'user_id',];

   public function users() {
   	return $this->belongsTo('App\User', 'user_id');
   }
}


