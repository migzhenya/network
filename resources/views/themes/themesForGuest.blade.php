@extends('layouts.app')
@section('content')


	<table class='table table-hover table-bordered table-striped' id="themes">
	<tr>
		<td>Заголовок/автор</td>
		<td>Комментов/просмотров</td>
		<td>Последний коммент от</td>
	</tr>
	<tr>
		<td >
			<a href="{{asset('themes/onetheme')}}">Обработка данных из формы PhP.</a>
			Автор: Archi2880
		</td>
		<td>
			Комментов: <a href="#">2017</a>
			Просмотров: 10030
		</td>
		<td>
			<a href="#">Miha2015</a>
			15 Марта 2016 в 10:07
		</td>
	</tr>
	<tr>
		<td>
			<a href="#">Печём капустные оладьи в микроволновке.</a>
			Автор: Alena71
		</td>
		<td>
			Комментов: <a href="#">2017</a>
			Просмотров: 10030
		</td>
		<td>
			<a href="#">EdgarPo</a>
			24 апреля 1973
		</td>

	</tr>
	<tr>
		<td>
			<a href="#">Путешествуем автостопом по космосу</a>
			Автор: raketa90
		</td>
		<td>
			Комментов: <a href="#">2017</a>
			Просмотров: 10030
		</td>
		<td>
			<a href="#">Alien3016</a>
			12 сентября 2067
		</td>
	</tr>
	</table>
	<a href="login">Войдите</a> или <a href="register">Зарегистрируйтесь</a>, чтобы добавить свою тему.
           
@endsection
