@extends('layouts.app')
@section('content')
 <form  class="form-inline" action="{{asset('usercontents')}}" method="POST" enctype="multipart/form-data">
  {!! csrf_field() !!}
  <div class="select_contents">
	<div class="col-md-8">
	<select class="form-control" name="theme_for_contents">

	@foreach ($themes as $key=>$theme)
		<option value="{{$theme->id}}">{!!$theme->name!!}</option>
		
	@endforeach
	</select>
	</div>
	<div class="col-md-4">

	<a href="themes" class="btn btn-default">Добавить тему</a>
	</div>
	</div>
	<div class="add_contents">
	  <!-- Nav tabs -->
	  <ul class="nav nav-tabs" role="tablist">
	    <li role="presentation" class="active"><a href="#article" aria-controls="article" role="tab" data-toggle="tab">Статья</a></li>
	    <li role="presentation"><a href="#photo" aria-controls="photo" role="tab" data-toggle="tab">Фото</a></li>
	    <li role="presentation"><a href="#audio" aria-controls="audio" role="tab" data-toggle="tab">Аудио</a></li>
	    <li role="presentation"><a href="#video" aria-controls="video" role="tab" data-toggle="tab">Видео</a></li>
	    <li role="presentation"><a href="#file" aria-controls="file" role="tab" data-toggle="tab">Файл</a></li>
	    <li role="presentation"><a href="#url" aria-controls="url" role="tab" data-toggle="tab">Ссылка</a></li>

	  </ul>

	  <!-- Tab panes -->
	  <div class="tab-content">
	    <div role="tabpanel" class="tab-pane active" id="article">
		   
			   
				<div>
					<div class="col-md-12">
						<textarea  name="article" placeholder="Статья полностью"></textarea>
					</div>
				</div>

				
	   		
	    </div>
	    <div role="tabpanel" class="tab-pane" id="photo">
	    	<div class="form-group">
				<div class="col-md-12">
				<h3>Вы можете добавить фото...</h3>
					<input type="file" class="form-control" name="photografy" >
				</div>
			</div>
	    </div>
	    <div role="tabpanel" class="tab-pane" id="audio">
	    	<div class="form-group">
				<div class="col-md-12">
				<h3>Добавьте музыку и потом слушайте то, что нравится вам!</h3>
					<input type="file" class="form-control" name="audio" >
				</div>
			</div>
	    </div>
	    <div role="tabpanel" class="tab-pane" id="video">
	    	<div class="form-group">
				<div class="col-md-12">
				<h3>Добавьте видеоролики, используя кнопку Обзор...</h3>
					<input type="file" class="form-control" name="video" >
				<h3>Или укажите ссылку на видео.</h3>
					<input type="text" class="form-control" name="video" >
				</div>
			</div>
	    </div>
	    <div role="tabpanel" class="tab-pane" id="file">
	   		<div class="form-group">
				<div class="col-md-12">
				<h3>Загружайте файлы, которыми хотите поделиться с друзьми!</h3>
					<input type="file" class="form-control" name="file" >
				</div>
			</div>
	    </div>
	    <div role="tabpanel" class="tab-pane" id="url">
	    	<div class="form-group">
				<div class="col-md-12">
				<h3>Делитесь ссылками на проверенные сайты.</h3>
					<input type="text" class="form-control" name="url" >
				</div>
			</div>
	    </div>

	  </div>

	</div>	       		
		   		

<div class="clear"></div>
 <button type="submit" id="contents_submit" class="btn btn-primary">
        <i class="fa fa-btn fa-sign-in"></i>Добавить
    </button>
</form>

   

@endsection
