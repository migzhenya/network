@extends('layouts.app')
@section('content')

	<form method="POST" action="{{asset('articles')}}" class="form-inline">
                        {!! csrf_field() !!}

		<div class="form-group">
			<div class="col-md-8">
				<input type="text" class="form-control" name="name" placeholder="Название статьи">
			</div>
		</div>
		
		<div class="form-group">
			<div class="col-md-8">
				<input type="text" class="form-control" name="keywords" placeholder="Ключевые слова">
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-8">
				<input type="text" class="form-control" name="slug" placeholder="Алиас">
			</div>
		</div>
		<br>
		<div>
			<div class="col-md-12">
				<textarea  name="body" placeholder="Статья полностью"></textarea>
			</div>
		</div>
		<br>
		<div class="form-group">
       		<div id="themes_submit" >
	            <button type="submit" id="themes_submit" class="btn btn-primary">
	                <i class="fa fa-btn fa-sign-in"></i>Добавить
	            </button>
        	</div>
   		</div>
	</form>


	<table class='table table-hover table-bordered table-striped' id="themes">
	<tr>
		<td>Название</td>
		<td>Комментов/просмотров</td>
		<td>Последний коммент от</td>
		<td>Опции</td>
	</tr>
	@foreach ($articles as $key => $article)
	<tr>
		<td >
			<a href="{{asset('themes/onetheme/'.$article->slug)}}">{!!$article->name!!}</a>
			Автор: {{--{!!$theme->users->name!!}--}}
		</td>
		<td>
			Комментов: <a href="#">2017</a>
			Просмотров: 10030
		</td>
		<td>
			<a href="#">Miha2015</a>
			15 Марта 2016 в 10:07
		</td>
		<td>
			<a href="#"><img src="{{asset('media/img/edit.png')}}" class="options"/></a>
			<a href="{{asset('themes/delete/'.$theme->id)}}"><img src="{{asset('media/img/delete.png')}}" class="options"/></a>

		</td>
	</tr>
	@endforeach
	</table>

@endsection
