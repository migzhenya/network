@if($url=='/cabinet/personaldata')
	<div class="link_view">Личные данные</div>
@else
	<a href="{{asset('cabinet/personaldata')}}" class="cabinet_links">Личные данные</a>
@endif



@if($url=='/themes')
	<div class="link_view">Мои темы</div>
@else
	<a href="{{asset('themes')}}" class="cabinet_links">Мои темы</a>
@endif

@if($url=='/usercontents')
	<div class="link_view">Мой контент</div>
@else
	<a href="{{asset('usercontents')}}" class="cabinet_links">Мой контент</a>
@endif

@if($url=='/friends')
	<div class="link_view">Друзья</div>
@else
	<a href="{{asset('friends')}}" class="cabinet_links">Друзья</a>
@endif

@if($url=='/group')
	<div class="link_view">Мои группы</div>
@else
	<a href="{{asset('group')}}" class="cabinet_links">Мои группы</a>
@endif

@if($url=='/cabinet/messages')
	<div class="link_view">Сообщения</div>
@else
	<a href="{{asset('cabinet/messages')}}" class="cabinet_links">Сообщения</a>
@endif



<div class="right_bread">
	<a href="/cabinet">Кабинет</a>
	<a href="/logout">Выход</a>
	
</div>