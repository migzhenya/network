<div class="cabinet_sidebar">
   
<div id="name">
    <p>{{$personal->fio}}</p>
     <div id="avatar">
        @if($personal->avatar)
            <img src="{{asset('/media/uploads/avatar/'.$personal->user_id.'/s_'.$personal->avatar)}}"/>
        @else
            <img src="{{asset('media/img/empty.jpg')}}" id="empty_pic"/>
        @endif
    </div>
    <div class="service_info">
        <p>Статус: {{$personal->status}}</p>
        <p>Рейтинг: {{$personal->raiting}}</p>
        <p>Дата регистрации: {{$personal->created_at}}</p>
    </div>
</div>

        <a href="#">Мои новости</a>
        <a href="#">Мои друзья</a>
        <a href="#">Моя музыка</a>
        <a href="#">Моё видео</a>
        <a href="#">Мои фотографии</a>
        <a href="#">Мои подписки</a>
        
        <a href="{{asset('cabinet/settings')}}" class="service">Настройки аккаунта</a>
    </div>